# Anvil

The base extensions, tasks, and conventions for building projects in the Wurk ecosystem. Our custom build tasks are implemented through this package's `.targets` file, and injected into the dependent project by installing the Nuget package. This allows us to apply our build and packaging conventions automatically, and additionally apply instrumentation and custom metadata to the built assemblies.

For more information on using this package, and others that support continuous development, please see the [Wurk ecosystem](http://wurk.io/technology/ecosystem/).

Libraries built with this package included will have their `AssemblyVersion` and Nuget package versions set automatically according to the configured GitVersion settings,

### Include this in your project

Simply install via the Nuget package manager &ndash;
```
PM> Install-Package Wurk.Anvil
```

The Wurk Nuget package library is available at https://packages.wurk.io/nuget, and should be added to your configured sources before running the `Install-Package` command.

### Configure after installation
Installing the package will add a `anvil.config` file to your project, which allows you to configure or disable features. The default configuration is as follows &ndash;

```xml
<?xml version="1.0" encoding="utf-8"?>
<Anvil>
  <Author>Max Awesome</Author>
  <Owners>Awesome Co</Owners>
  <Culture UseUserCulture="false">en-za</Culture>
  <License href="http://dwtfywtd.license/" />
  <Project Name="Awesome Sauce" href="http://someawesome.co/" />
  <GitVersion />
  <AssemblyInfo UseMetadata="true" />
  <PackageManifest Name="Awesome.Package" Generate="true" IncludeDependencies="true" IncludeReadme="true" />
</Anvil>
```

### Configuration Elements
#### `Author`
The author name to be used in the generated `.nuspec` and `AssemblyInfo`

#### `Owners`
The owners' name to be used in the generated `.nuspec` and `AssemblyInfo`

#### `Culture`
The content of this element is used to indicate the assembly and package's default culture.
><dl>
  <dt>UseUserCulture</dt>
  <dd>
    `true` or `false`. If `true` the text content may be omitted.  </dd>
</dl>

#### `License`
><dl>
  <dt>href</dt>
  <dd>
	The URL of the package's license
  </dd>
</dl>

#### `Project`
><dl>
  <dt>Name</dt>
  <dd>
	The project's name
  </dd>
  <dt>href</dt>
  <dd>
	The project's website
  </dd>
</dl>
 
#### `GitVersion`
Adds a GitVersion task to the project, versioning the assembly based on semver and branching conventions. To disable versioning features, simply remove this element.