﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle( "Anvil" )]
[assembly:
    AssemblyDescription( "The base extensions, tasks, and conventions for building projects in the Wurk ecosystem." )]
[assembly: AssemblyCompany( "Wurk" )]
[assembly: AssemblyProduct( "Anvil" )]
[assembly: AssemblyCopyright( "Copyright - Wurk, 2016" )]
[assembly: AssemblyCulture( "" )]
[assembly: ComVisible( false )]
[assembly: Guid( "1345a10c-d833-4f69-b913-0cc53293d547" )]